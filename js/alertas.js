function Pantalon1(){
    Swal.fire({
        title: 'Pans Ahegao',
        text: 'Bonito Pans Con Diseño Ahegao Perfecto Para Dormir',
        imageUrl: 'assets/pantalones/pantalon1.png',
        backdrop: true,
    })
};

function Pantalon2(){
    Swal.fire({
        title: 'Pans Asce B',
        text: 'Bonito Pans Con Diseño De Asce Perfecto Para Dormir',
        imageUrl: 'assets/pantalones/pantalon2.png',
        backdrop: true,
    })
};

function Pantalon3(){
    Swal.fire({
        title: 'Pans Asce W',
        text: 'Bonito Pans Con Diseño De Asce Perfecto Para Dormir',
        imageUrl: 'assets/pantalones/pantalon3.png',
        backdrop: true,
    })
};

function Sudadera1(){
    Swal.fire({
        title: 'Sudadera Ahegao',
        text: 'Bonita Sudadera Con Diseño Ahegao Perfecta Para Esas Noches Frías',
        imageUrl: 'assets/sudaderas/sudadera1.png',
        backdrop: true,
    })
};

function Sudadera2(){
    Swal.fire({
        title: 'Sudadera Anime',
        text: 'Bonita Sudadera Con Diseño Anime Perfecta Para Esas Noches Frías',
        imageUrl: 'assets/sudaderas/sudadera2.jpg',
        backdrop: true,
    })
};

function Sudadera3(){
    Swal.fire({
        title: 'Sudadera Bakugo',
        text: 'Bonita Sudadera Con Diseño De Bakugo Perfecto Para Esas Noches Frías',
        imageUrl: 'assets/sudaderas/sudadera3.png',
        backdrop: true,
    })
};

function Playera1(){
    Swal.fire({
        title: 'Playera Letras',
        text: 'Bonita Playera Con Estampado De Letras Japonesas',
        imageUrl: 'assets/playeras/playera1.jpg',
        backdrop: true,
    })
};

function Playera2(){
    Swal.fire({
        title: 'Playera Nezuko Chan',
        text: 'Bonita Playera Con Estampado De Nezuko Chan',
        imageUrl: 'assets/playeras/playera2.jpg',
        backdrop: true,
    })
};

function Playera3(){
    Swal.fire({
        title: 'Playera Zero Two',
        text: 'Bonita Playera Con Estampado De Zero Two',
        imageUrl: 'assets/playeras/playera3.webp',
        backdrop: true,
    })
};

function Tenis1(){
    Swal.fire({
        title: 'Tenis Goku',
        text: 'Tenis De Dragon Ball Super',
        imageUrl: 'assets/zapatos/zapatos1.jpg',
        backdrop: true,
    })
};

function Tenis2(){
    Swal.fire({
        title: 'Tenis Anime',
        text: 'Tenis Con Estampado De Anime',
        imageUrl: 'assets/zapatos/zapatos2.jpg',
        backdrop: true,
    })
};

function Tenis3(){
    Swal.fire({
        title: 'Tenis Jojo',
        text: 'Tenis Diseño Jojos',
        imageUrl: 'assets/zapatos/zapatos3.jpg',
        backdrop: true,
    })
};

function Shorts1(){
    Swal.fire({
        title: 'Shorts Goku Ultra Instic W',
        text: 'Shorts Con Diseño De Goku Ultra Instinto Blanco',
        imageUrl: 'assets/shorts/shorts1.webp',
        backdrop: true,
    })
};

function Shorts2(){
    Swal.fire({
        title: 'Shorts Kamehameha',
        text: 'Shorts Con Diseño Kamehameha',
        imageUrl: 'assets/shorts/shorts2.webp',
        backdrop: true,
    })
};

function Shorts3(){
    Swal.fire({
        title: 'Shorts Goku Ultra Instic B',
        text: 'Shorts Con Diseño De Goku Ultra Instinto Negro',
        imageUrl: 'assets/shorts/shorts3.webp',
        backdrop: true,
    })
};

function Sueter1(){
    Swal.fire({
        title: 'Sueter De Mona China',
        text: 'Sueter Con Diseño De Mona China Negro',
        imageUrl: 'assets/sueter/sueter1.jpg',
        backdrop: true,
    })
};

function Sueter2(){
    Swal.fire({
        title: 'Sueter Death Note',
        text: 'Sueter Con Diseño Del Anime Death Note Negro',
        imageUrl: 'assets/sueter/sueter2.jpg',
        backdrop: true,
    })
};

function Sueter3(){
    Swal.fire({
        title: 'Sueter Zenitsu Agatsuma',
        text: 'Sueter Con Diseño De Zenitsu Agatsuma De Demon Slayer Negro',
        imageUrl: 'assets/sueter/sueter3.jpg',
        backdrop: true,
    })
};
/*Opciones de configuracion*/
    // title:   Titulo
	// text:    Texto para rellenar
	// html:    Sirve para poner segmentos html
	// icon:    Iconos 'success' 'error' 'warning' 'info' 'question'
	// confirmButtonText:   cambiar texto del boton
	// footer:  Agregar pie de pagina de la alerta
	// width:   Ancho (No recomendado)
	// padding:
	// background:  Cambiar el fondo '#000'
	// grow:    'row' 'column' 'fullscreen' 
	// backdrop:    'true' 'false'    Sirve para que la pantalla de atras se ponga un poco osura
	// timer: 5000   Tiempo que permanece activa
	// timerProgressBar:    'true' 'false' Sirve para una barra progresiva
	// toast:
	// position:
	// allowOutsideClick:
	// allowEscapeKey:
	// allowEnterKey:
	// stopKeydownPropagation:

	// input:
	// inputPlaceholder:
	// inputValue:
	// inputOptions:
	
	//  customClass:
	// 	container:
	// 	popup:
	// 	header:
	// 	title:
	// 	closeButton:
	// 	icon:
	// 	image:
	// 	content:
	// 	input:
	// 	actions:
	// 	confirmButton:
	// 	cancelButton:
	// 	footer:	

	// showConfirmButton:
	// confirmButtonColor:
	// confirmButtonAriaLabel:

	// showCancelButton:
	// cancelButtonText:
	// cancelButtonColor:
	// cancelButtonAriaLabel:
	
	// buttonsStyling:
	// showCloseButton:
	// closeButtonAriaLabel:


	// imageUrl:
	// imageWidth:
	// imageHeight:
	// imageAlt: